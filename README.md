# MOSAIX

MatLab tool to build FITS mosaics from GALEX images.

<a href="https://www.mathworks.com/"><img src="https://img.shields.io/badge/MatLab-100%25-green"></a>

![example](M31fuv.jpg)

## Documentation

Open doc.pdf file.

## Usage

Run in MatLab next commands:

```
>> totalmerging('M31fuv','test.fits',0)
>> [ilin,ilog] = fits_load('test.fits');
>> imshow(ilog)
>> imwrite("test.jpg",ilog)
```
And open test.jpg file. FITS write is enabled (see doc.pdf) to write ilin image with original source linear signal.

## License and cites

Original paper reference:

```
	@article{armengot2014,
		author		= {Marcelo Armengot and Néstor Sánchez and Javier López-Santiago and AnaInés Gómez de Castro},
		journal		= {Astrophysics and Space Science},
		title		= {MOSAIX: a tool to build large mosaics from GALEX images},
		year		= {2014},
		volume		= {352},
		number		= {2},
		issn		= {0004-640X},
		doi		= {10.1007/s10509-014-1997-5},
		url		= {http://dx.doi.org/10.1007/s10509-014-1997-5},
		publisher	= {Springer Netherlands},
		keywords	= {Astronomical images; Image processing; Space telescopes; Ultraviolet astronomy},
		language	= {English}}
```