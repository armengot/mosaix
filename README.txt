(0) Mosaix scripts v2.0
    Copyright (C) 2014 
    Marcelo Armengot, Nestor Sánchez Doreste
    Javier López Santiago, Ana Inés Gómez de Castro

(1) BibTeX reference:
	@article{armengot2014,
		author		= {Marcelo Armengot and Néstor Sánchez and Javier López-Santiago and AnaInés Gómez de Castro},
		journal		= {Astrophysics and Space Science},
		title		= {MOSAIX: a tool to built large mosaics from GALEX images},
		year		= {2014},
		volume		= {352},
		number		= {2},
		issn		= {0004-640X},
		doi		= {10.1007/s10509-014-1997-5},
		url		= {http://dx.doi.org/10.1007/s10509-014-1997-5},
		publisher	= {Springer Netherlands},
		keywords	= {Astronomical images; Image processing; Space telescopes; Ultraviolet astronomy},
		language	= {English}}

(2) Mosaix is a set of MatLab(tm) scripts to build big mosaics from GALEX
    images.

    You can check the software generating a first mosaic:

    	>> totalmerging('M31fuv/','test.fits',0)

    After executing the above command test the new 'test.fits' FITS file
    in a FITS viewer as ds9 or others.

(3) You will find each script help typing 'help <script>' in the MatLab(tm)
    prompt.

(4) Mosaix set of scripts is available in the mosaix.tar.gz file which provides
    next files:
	.
	├── wfits
	│   ├── addLine.m
	│   ├── cell2str.m
	│   ├── fits_write.m
	│   ├── getPrecision.m
	│   ├── makeHeaderLine.m
	│   ├── padFile.m
	│   ├── requiredKeywords.m
	│   ├── struct2str.m
	│   ├── validate.m
	│   └── LICENSE.txt
	├── M31fuv
	│   ├── NGA_M31_F1-fd-int.fits
	│   ├── NGA_M31_F3-fd-int.fits
	│   ├── NGA_M31_MOS0-fd-int.fits
	│   ├── NGA_M31_MOS17-fd-int.fits
	│   ├── NGA_M31_MOS4-fd-int.fits
	│   ├── PS_M31_MOS00-fd-int.fits
	│   └── PS_M31_MOS10-fd-int.fits
	├── cellindexnonull.m
	├── cleanimg.m
	├── degreescanvas.m
	├── distancetonorthpole.m
	├── fits_load.m
	├── fits_save.m
	├── keywordvalue.m
	├── lightmerging.m
	├── morphocleanimg.m
	├── pixelcanvas.m
	├── radec.m
	├── radectopixel.m
	├── totalmerging.m
	├── zmax.m
	├── zrate.m
	├── README.txt
	└── LICENSE.txt

     The M31fuv folder provides a set of M31 galaxy FITS for testing the software.

(5)  If you downloaded the mosaix_scripts.tar.gz you can download your own 
     FITS images from GALEX for mosaicking.

(6)  The mfits package (developed by Jason Fiege) is included in the wfits folder
     with certain modifications and its own license.



