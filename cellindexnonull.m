function i = cellindexnonull(cell_booleans)
% CELLINDEXNONULL
%   extract index from a certain keyword of the FITS data header
%
    [n,tmp]=size(cell_booleans);
    k=1;
    while (isempty(cell_booleans{k}))        
        k=k+1;
    end;
    i=k;
end
