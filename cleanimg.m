function [I,template] = cleanimg(I,hdu)
% CLEANIMG
%   [output,template] = cleanimg(input image, header)
%   
%   GALEX images are round.
%   This function write NaN values to the surrounding area which
%   have 0 values.
%
%   INPUT PARAMETERS:
%   +input image: double or single matrix
%   +header: FITS hdu values
%
%   OUTPUT VALUES:
%   +output image: the image with NaN values in the surrounding area
%   +template: logical matrix with the same size than the output
%   image and 1 in the NaN area and 0 in the middle circle
%
    band=keywordvalue('BAND',hdu);
    % band
    % 1=nuv,2=fuv    
    if (band==1)
        d=(1.14*3840/1.6)/2; % NUV GALEX 1.24
    else        
        d=(1.14*3840/1.6)/2; % FUV GALEX 1.28
    end    
    % fix the real center
    radex(1)=keywordvalue('RA_CENT',hdu);
    radex(2)=keywordvalue('DEC_CENT',hdu);
    avaspra=keywordvalue('AVASPRA',hdu);
    avasdec=keywordvalue('AVASPDEC',hdu);
    [X,Y,LINE,SAMPLE,A,F]=radectopixel([avaspra avasdec],radex,[1920.5 1920.5]);
    [rows cols]=size(I);
    [rr cc] = meshgrid(1:rows);
    rr=rr+round(LINE);
    cc=cc+round(SAMPLE);
    template = sqrt((rr-rows/2).^2+(cc-cols/2).^2)<=d;    
    I(~template)=nan;        
end
