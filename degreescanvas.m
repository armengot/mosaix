function [plotview,central] = degreescanvas(radex)
% DEGREESCANVAS
%   [field of view, central pixel] = DEGREESCANVAS(radex)
%   
%   Compute the field of view for the merging canvas in degrees.
%    
    radelta=max(radex(:,1))-min(radex(:,1));
    decelta=max(radex(:,2))-min(radex(:,2));

    central(1)=min(radex(:,1))+((max(radex(:,1))-min(radex(:,1)))/2);
    central(2)=min(radex(:,2))+((max(radex(:,2))-min(radex(:,2)))/2);
    
    plotview(1)=radelta;
    plotview(2)=decelta;

end

