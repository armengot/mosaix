function [LINE, SAMPLE] = distancetonorthpole(RADEC)
% DISTANCETONORTHPOLE 
%   distancetonorthpole(radec) returns the distance between a point sited
%   on Right Ascension = radec(1) and Declination = radec(2) and the North
%   Pole. The value is returned in degrees.

    % image center
    alpha0=RADEC(1);
    delta0=RADEC(2);
        
    % North Pole
    alpha=alpha0;
    delta=90;
        
    % scale
    scale=3840/1.6;
    
    % gnomonic projection equations
    A=cosd(delta) * cosd(alpha-alpha0);
    F=scale * (180/pi)/((sind(delta0)*sind(delta))+(A*cosd(delta0)));
    LINE=-F*((cosd(delta0)*sind(delta))-(A*sind(delta0)));
    SAMPLE=-F*(cosd(delta)*sind(alpha-alpha0));

    LINE=abs(LINE);
end

