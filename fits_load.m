function [input_lin,input_log]=fits_load(pathchar)
% FITS_LOAD
%   [ilin, ilog] = FITS_LOAD (path)
%
%   Read FITS file from the input path and provide two outputs:
%   +ilin: image matrix in lineal (original) values
%   +ilog: image matrix in logarithmic scaled values
%
    img=fitsread(pathchar);
    input_lin=img(end:-1:1,:);
    % =========================
    a=1000;
    input_log=log(a.*input_lin+1)./log(a);

end
