function fits_save(filename,input,header)
% FITS_SAVE 
%   FITS_SAVE (filename, input, header)
%  
%   INPUT
%   + filename: destiny path for writing the output
%   + input: image matrix
%   + header: FITS header values
%
%   It uses MFITS library for writing FITS
%   mfits's library author: Jason D. Fiege, University of Manitoba, 2010
%   fiege@physics.umanitoba.ca
%   
    addpath('wfits');
    
    if (exist('header'))
        fits_write(filename,input,header);
    else
        fits_write(filename,input);
    end
end
