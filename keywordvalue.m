function [value] = keywordvalue(keyword,header)
% KEYWORDVALUE
%   [ output ] = KEYWORDVALUE ( keyword , header )
%
%   INPUT:
%   + keyword: name of the field in the HDU
%   + header: HDU from the FITS file
%
%   OUTPUT:
%   + output: value in the HDU for each keyword
%
%   Note that header =  hdu.PrimaryData.Keywords;
%   KEYWORDVALUE function only extracts a value from the image FITS 
%   header.
%
    hdu=header; 
	i=cellindexnonull(strfind(hdu(:,1),keyword));	
	tmp=hdu(i,2);
	value=tmp{1};
end

