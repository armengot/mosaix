% LIGHTMERGING complete function
% ************
% Galex FITS merging in MOSAIX scripts set
% Low memory requirements (version 2)
%
%   [mosaic, centerpx, centeradec] = LIGHTMERGING(names)
%
%   INPUT:
%   + list of file names: example
%     names{1} = '/path/folder/file1.fits'
%     names{2} = '/path/folder/file2.fits'
%
%   OUTPUT:
%   + mosaic: image matrix
%   + centerpx: (X,Y) central pixel
%   + centeradec: Right Ascension and Declination of centerpx
%   pixel
function [mosaic,centerpx,centeradec]=lightmerging(names)        
    % n names of the files 
    n=size(names,2);
    % Extract RAs and DECs and make the canvas    
    radex=zeros(n,2);
    clear firstheader
    for i=1:n
        completehdu=fitsinfo(names{i});
        hdu{i}=completehdu.PrimaryData.Keywords;
        radex(i,1)=keywordvalue('RA_CENT',hdu{i});
        radex(i,2)=keywordvalue('DEC_CENT',hdu{i});
        average(i,1)=keywordvalue('NAXIS1',hdu{i});
        average(i,2)=keywordvalue('NAXIS2',hdu{i});
        texp(i)=keywordvalue('EXPTIME',hdu{i});
        band(i)=keywordvalue('BAND',hdu{i}); % 1=nuv,2=fuv
        avaspra(i)=keywordvalue('AVASPRA',hdu{i});
        avasdec(i)=keywordvalue('AVASPDEC',hdu{i});
        clear completehdu
    end;    
    texps=cumsum(texp);
    Tcom=texps(end);
    [region,centeradec]=degreescanvas(radex);
    [canvas,centerpx]=pixelcanvas(region,names,300);
    canvas(:,:)=nan;
    %canvas(:,:,2)=nan;
    clear region
    if (size(canvas,1)>min(average(1,1))) % check canvas
        % put each fits in its position        
        d=distancetonorthpole(centeradec);    
        for i=1:n                
            [dx dy line sample]=radectopixel([radex(i,1) radex(i,2)],centeradec,centerpx);
            opposite_leg=abs(sample);
            adjacent_leg=d-abs(line);        
            angle=atand(opposite_leg/adjacent_leg);
            if (centeradec(1)>radex(i,1))
                angle=abs(angle);
            else
                angle=-1*abs(angle);
            end;  
            % ============================
            % load image	    
            zimg=fitsread(names{i});
            wimg=single(zimg(end:-1:1,:));    
            clear zimg            
            img=(cleanimg(wimg,hdu{i}));
            %img=wimg;
            clear wimg zimg
            % ============================
            [sizey sizex]=size(img);
            halfx=round(sizex/2);
            halfy=round(sizey/2);
            dx=dx-halfx;
            dy=dy-halfy;
            %canvas(dx:dx+sizex-1,dy:dy+sizey-1,2)=imrotate(locali,angle,'bilinear','crop');% check canvas
            % se ha desglosado en dos lineas
            % lo que viene a continuacion
            
            % (1) [cleaned,template]=morphocleanimg(imrotate(img,angle,'nearest','crop'));
            % (2) 
            [cleaned,template]=cleanimg(imrotate(img,angle,'nearest','crop'),hdu{i});
                        
            % (3) cleaned=imrotate(cleanimg(img),angle,'nearest','crop');
            %cleaned=cleaned.*(Tcom-texp(i));
            
            canvas(dx:dx+sizex-1,dy:dy+sizey-1)=nansum(cat(3,cleaned,canvas(dx:dx+sizex-1,dy:dy+sizey-1)),3)./sum(~isnan(cat(3,canvas(dx:dx+sizex-1,dy:dy+sizey-1),cleaned)),3);
            % first version
            % canvas(dx:dx+sizex-1,dy:dy+sizey-1,2)=cleanimg(imrotate(img,angle,'nearest','crop'));% check canvas            
            clear img cleaned
            %canvas(:,:,1)=nansum(canvas,3)./sum(~isnan(canvas),3);
            %mosaic(isnan(mosaic))=0;
            %canvas(:,:,1)=mosaic;            
            %canvas(:,:,2)=nan;%zeros(size(canvas(:,:,2)));            
            
            display(strcat([' + Processed ' num2str(i) ' image (file: <strong>' names{i} '</strong>)']))
            display(strcat(['   Distance from center (' num2str(dx) ', ' num2str(dy) ') px.']))
            display(strcat(['   Rotation angle ' num2str(angle) ' degrees.']))
        end;
    else
        display(' + Process of matching broken off, error in FITS files.');
    end;
    %mosaic=sum(im2double(canvas),3)./sum(canvas~=0,3);
    %mosaic(isnan(mosaic))=0;
    % mosaic=canvas(:,:,1);
    mosaic=canvas(end:-1:1,:); %./Tcom;
    %mosaic(isnan(mosaic))=min(mosaic(:));
end
