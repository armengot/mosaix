% MORPHOCLEANIMG
%   [ output, template ] = MORPHOCLEANIMG (input image)
%
%   See cleanimg function before. This function writes the
%   NaN surrounding area, here it does not depend on the
%   radius but also the morphologic form of the image.
%
%   INPUT PARAMETERS:
%   +input image: double or single matrix
%
%   OUTPUT VALUES:
%   +output image: the image with NaN values in the surrounding area
%   +template: logical matrix whith the same size than the output
%   image and 1 in the NaN area and 0 in the middle circle
%
function [I,template] = morphocleanimg(I)
    Q=255.*(I~=0);
    w1=strel('disk',7);
    w2=strel('disk',8);
    O=imdilate(Q,w1);
    P=imerode(O,w2);
    template=~P;
    
    % for testing
    %template=~P;
    %J=I+min(I(:));
    %K=255.*J./max(J(:));
    %Z(:,:,1)=255.*template;
    %Z(:,:,2)=K;
    %Z(:,:,3)=0;
    %figure,imshow(Z)
    % K(template)=nan;    
    
    I(template)=nan;
    
    
