% PIXELCANVAS
%   [canvas,centerpx] = PIXELCANVAS(radec,names,frame)
%
%   Compute the output canvas (size) of the final mosaic using
%   a set of input parameters.
%
%   INPUT:
%   + radec: array of RA and DEC from the set of images
%   + names: array of file names 
%   + frame: size of safety frame
%
%   OUTPUT:
%   + canvas: matrix to write the mosaic
%   + centerpx: central pixel in the canvas
%
function [canvas,centerpx]=pixelcanvas(radecanvas,names,frame)
    smallmargin=1;
    n=size(names,2);
    test=1;    
    % checking squared images and every one with same degrees
    for i=1:n
        completehdu=fitsinfo(names{i});
        hdu=completehdu.PrimaryData.Keywords;
        if (keywordvalue('NAXIS1',hdu) ~= (keywordvalue('NAXIS2',hdu)))
            test=0;
        else
            pixels=keywordvalue('NAXIS1',hdu);
        end;
        ratio(i)=keywordvalue('CDELT2',hdu);
        if (i>1)
            if (ratio(i) ~= ratio(i-1))
                test=0;
            end;
        end;
    end;    
    if (test)
        scale=1/ratio(1);
        H=frame+pixels+ceil(radecanvas(1)*scale);
        W=frame+pixels+ceil(radecanvas(2)*scale);
        centerpx(1)=W/2;
        centerpx(2)=H/2;
        %canvas=uint8(zeros(W,H,n)); for fast result
        canvas=single(zeros(W,H,1));        
        display(' + Canvas successfully generated.');
        display(' + Every FITS with the same width and height (in pixel or degrees) as the first (checked).');
    else
        display(' + Non-square images or different degrees. Canvas was not generated.');
        canvas=[];
    end;
end

