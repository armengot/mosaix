% RADEC
%   extract RA and DEC coordinates from a data header
%   [ra dec] = RADEC (data_header)
%
function [ra,dec] = radec(data_header)
	ind_ra=cellindexnonull(strfind(data_header(:,1),'RA_CENT'));
	ind_dec=cellindexnonull(strfind(data_header(:,1),'DEC_CENT'));
	tmpra=data_header(ind_ra,2);
	tmpdec=data_header(ind_dec,2);
	ra=tmpra{1};
	dec=tmpdec{1};
end

