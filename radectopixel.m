function [X,Y,LINE,SAMPLE,A,F] = radectopixel(RADEC,CENTRALRADEC,CENTRALPIXEL)
% RADECTOPIXEL
%   [ x y line sample A F ] = RADECTOPIXEL(radec,centralradec,centralpixel)
%
%   Given a reference point (centralpixel) in the canvas with a certain 
%   RA and DEC stored in the CENTRALRADEC variable, computes the X Y 
%   coordinates from a certain distinct pint.
%
%   INPUT:
%   + radec: Right Ascension and Declination of the requested point
%   + centralradec: RA and DEC from the center of the image
%   + centralpixel: X and Y coordinates associated to the central RA DEC
%
%   OUTPUT:
%   + X,Y : output x y coordinates for the RADEC input point
%   + LINE : y-axis offset between Y output and Y value from 
%   the central pixel
%   + SAMPLE: x-axis offfset
%
%   More information:
%   http://lambda.gsfc.nasa.gov/product/iras/coordproj.cfm#gnomonic
%        
    scale=3840/1.6;
    alpha=RADEC(1);
    delta=RADEC(2);
    
    CX=CENTRALPIXEL(1);
    CY=CENTRALPIXEL(2);
    
    alpha0=CENTRALRADEC(1);
    delta0=CENTRALRADEC(2);
    
    A=cosd(delta) * cosd(alpha-alpha0);
    F=scale * (180/pi)/((sind(delta0)*sind(delta))+(A*cosd(delta0)));
    LINE=-F*((cosd(delta0)*sind(delta))-(A*sind(delta0)));
    SAMPLE=-F*(cosd(delta)*sind(alpha-alpha0));

    % Bug! 
    X=round(CX+LINE);
    Y=round(CY+SAMPLE);
    %Y=LINE;
    %X=SAMPLE;
end




