function totalmerging(folder,filename,showflag)
%                                     
%     _ __ ___   ___  ___  __ _( )_  __
%    | '_ ` _ \ / _ \/ __|/ _` | \ \/ /
%    | | | | | | (_) \__ \ (_| | |>  < 
%    |_| |_| |_|\___/|___/\__,_|_/_/\_\
%     _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
%   / Mosaicking (Galex) FITS v2.0    /
%   `·- - - - - - - - - - - - - - --·´ 
%
%    >> TOTALMERGING('folder/','filename.fits',0)
%
%       (1) Put the FITS files in the folder.
%       (2) Write as the second parameter the name of the output file.
%       (3) Third parameter (bit flag): it shows (or not) intermediate plot.
%
%       http://odin.estad.ucm.es/mosaix
%
%    TEST the scripts:
%    >> TOTALMERGING('M31fuv/','test.fits',0)
%    
%    MOSAIX is a software developed by the WSO-UV (Spain) team
%    Visit http://www.wso-uv.es for more information.
%    COMPLUTENSE UNIVERSITY OF MADRID
%
%    
%
%    =======================================================================
%
%    MOSAIX is a set of MatLab(tm) scripts to build big mosaics from GALEX
%    images. It was developed by the WSO-UV (Spain) team.
%    Copyright (C) 2014 Marcelo Armengot
% 
%    This program is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
% 
%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
% 
%    You should have received a copy of the GNU General Public License
%    along with this program.  If not, see http://www.gnu.org/licenses
%
%    =======================================================================
%
%    MOSAIX uses mfits library developed by Jason D. Fiege stored among
%    the Mosaix set in the wfits folder.
%    Jason D. Fiege, University of Manitoba, 2010
%    fiege@physics.umanitoba.ca
%
%    BSD license of Jason Fiege's mfits library is reproduced below.
%
%    =======================================================================
%
%    Copyright (C) 2010, Jason Fiege
%    All rights reserved.
% 
%    Redistribution and use in source and binary forms, with or without
%    modification, are permitted provided that the following conditions are
%    met:
% 
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
% 
%    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
%    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
%    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
%    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
%    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
%    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
%    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
%    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
%    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
%    POSSIBILITY OF SUCH DAMAGE.
    tic=clock;
	log=dir(folder);
	[n,tmp]=size(log);
	j=1;
	for i=1:n
		tmp=log(i);
		if (~tmp.isdir) 
			ending=tmp.name(length(tmp.name)-4:length(tmp.name));
			if (strcmp(ending,'.fits'))
				name{j}=strcat(strcat(folder,'/'),tmp.name);            
				header=fitsinfo(name{j});
				%hdu{j}=header.PrimaryData.Keywords;
				tmphdu=header.PrimaryData.Keywords;
				[ra(j),dec(j)]=radec(tmphdu);
				zimg=fitsread(name{j});
				wimg=imresize(zimg(end:-1:1,:),0.03);
				img(:,:,j)=single(255*(wimg/max(wimg(:)))); % im2uint8
				clear zimg wimg            
				tmp=zrate(tmphdu);
				imax(j)=zmax(tmphdu);
				irate(j)=imax(j)*tmp;
				j=j+1;         
				clear tmphdu
			end;    
		end;
	end;
	tac=clock;
	time_view=etime(tac,tic);    
    display(strcat([' + Time loading ' num2str(time_view) ' seconds.']))
    
    m=1;
    for i=1:(j-1)
        selection{m}=name{i};
        m=m+1;
    end
    
    if (exist('showflag'))
        if (showflag)
            plot(ra,dec,'ro')
            hold on
            grid on
            for i=1:(j-1)    
                text(ra(i),dec(i),name{i}(9:length(name{i})-5),'FontSize',10)
                %colormap(gray)
                image([ra(i) ra(i)+0.8],[dec(i) dec(i)+0.8],img(:,:,i));
            end;
            set(gca,'XDir','reverse')
            xlabel('RA','FontSize',18)
            ylabel('DEC','FontSize',18)
        end
    end
	[xl,centerpx,centeradec]=lightmerging(selection);
	tac=clock;
	time_merging=etime(tac,tic);
	clear tic tac
    display(strcat([' + Time processing the mosaic ' num2str(time_merging) ' seconds.']));
	    
    % ==========================================================
    % finalname=strcat([folder '.fits']);
    finalname=filename;
	
    fits_save(finalname,xl);
	header=fitsinfo(finalname);
	%hdu=fitsinfo(groupI{1});
	hdu=fitsinfo(selection{1});

	header.PrimaryData.Keywords(6,1)={'CRPIX1'};
	header.PrimaryData.Keywords(6,2)={centerpx(2)};
	header.PrimaryData.Keywords(7,1)={'CRPIX2'};
	header.PrimaryData.Keywords(7,2)={centerpx(1)};
	header.PrimaryData.Keywords(8,1)={'CRVAL1'};
	header.PrimaryData.Keywords(8,2)={centeradec(1)};
	header.PrimaryData.Keywords(9,1)={'CRVAL2'};
	header.PrimaryData.Keywords(9,2)={centeradec(2)};
	header.PrimaryData.Keywords(10,1)={'CDELT1'};
	header.PrimaryData.Keywords(10,2)={keywordvalue('CDELT1',hdu.PrimaryData.Keywords)};
	%header.PrimaryData.Keywords(10,2)={'-0.000416666666666667'};
	header.PrimaryData.Keywords(11,1)={'CDELT2'};
	header.PrimaryData.Keywords(11,2)={keywordvalue('CDELT2',hdu.PrimaryData.Keywords)};
	%header.PrimaryData.Keywords(11,2)={'0.000416666666666667'};
	
	header.PrimaryData.Keywords(12,1)={'EQUINOX'};
	header.PrimaryData.Keywords(12,2)={2000};
	header.PrimaryData.Keywords(13,1)={'EPOCH'};
	header.PrimaryData.Keywords(13,2)={2000};

	header.PrimaryData.Keywords(14,1)={'CROTA2'};
	header.PrimaryData.Keywords(14,2)={0};

	header.PrimaryData.Keywords(15,1)={'CTYPE1'};
	header.PrimaryData.Keywords(15,2)={'RA---TAN'};
	header.PrimaryData.Keywords(16,1)={'CTYPE2'};
	header.PrimaryData.Keywords(16,2)={'DEC--TAN'};

	header.PrimaryData.Keywords(17,1)={'COMMENT'};
	header.PrimaryData.Keywords(17,2)={'WSOUV Spain at UCM (Madrid) Nestor Sanchez-Doreste & Marcelo Armengot   '};
	header.PrimaryData.Keywords(18,1)={'COMMENT'};
	header.PrimaryData.Keywords(18,2)={'For more details, please, visit www.wso-uv.es/mosaix and write us for help.    '};
	header.PrimaryData.Keywords(19,1)={'END'};
	
	fits_save(finalname,xl,header);
end


