function hStr=addLine(hStr, hLine)
% Just adds a line to the header character array.
%
lenLine=length(hLine);
if lenLine > 80
    hLine=hLine(1:80);
    lenLine=80;
end
endLine=repmat(' ', 1, 80);
endLine(1:lenLine)=hLine;
hStr=[hStr; endLine];