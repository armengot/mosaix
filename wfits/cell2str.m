function  [hStr, hReq]=cell2str(header, szData)
% Generates a FITS header from a cell array in the same format as returned
% by MATLAB's fitsinfo function.  hReq is a list of *unused* mandatory fields
% in the FITS standard.
[nr,nc]=size(header);
hReq=requiredKeywords(szData);
hStr='';
for i=1:nr
    keyword=header{i,1};
    value=header{i,2};
    %
    if nc > 2 && ~isempty(header{i,3})
        comment=strtrim(char(header{i,3}));
    else
        comment='';
    end
    %
    % Generate a single header line and append it to hStr.  Note that the
    % mandatory FITS fields in hReq are removed as they are used.  This
    % will only contain unused fields at the end.
    [hLine, hReq]=makeHeaderLine(keyword, value, comment, hReq);
    hStr=[hStr; hLine]; %#ok
end