function fits_write(filename, data, varargin)
global BITPIX transposeRC
%
% mfits: A simple fits package fo MATLAB.
%
% Author: Jason D. Fiege, University of Manitoba, 2010
% fiege@physics.umanitoba.ca
%
% The main component is a FITS writer fits_write to complement MATLAB's
% built-in fits reader.  fits_read is just a wrapper for fitsread.  fits_info
% returns the header information in a more convenient form than the
% built-in fitsinfo function.  Additional functionality is provides by
% getFitsValue and setFitsValue to get and set a single header field.
%
% -------------------------
% Usage:
% -----
% 1st argument: filename: string containing the name of the file to write.  '.fits' will
% be appended if the file name does not end in '.fits' or '.FITS'.
%
% 2nd argument: data: an N-dimensional array, usually of 32-bit floats or 64-bit doubles.
% Complex data types are not currently supported, but this may be added in
% the the near future.
%
% 3rd argument: header (optional): A fits header can be optionally supplied
% in one of 3 formats.
%
% Header format #1: The header is a structure loaded directly from MATLAB's
% fitsinfo command: header=fitsinfo('myFITS.fits');  In this case, the
% keyword/values are held in a cell array: header.PrimaryData.Keywords.  This
% cell array is extracted and header format #2 is used.  For example, if
% 'myFITS.fits' is a fits file, then the following commands read it in and
% re-write identical data with a copy of the same header:
%
% data=fitsread('myFITS.fits');
% header=fitsinfo('myFITS.fits');
% fitswrite('myFITS_copy.fits', data, header);
%
% Header format #2: The header may be a cell array, in the same format
% as the headers returned by MATLAB's fitsinfo command in the
% header.PrimaryData.Keywords field.  For example, if 'myFITS.fits' is a fits
% file, then the following commands read it in and re-write identical data
% with a copy of the same header:
%
% data=fitsread('myFITS.fits');
% header=fitsinfo('myFITS.fits');
% fitswrite('myFITS_copy.fits', data, header.PrimaryData.Keywords);
%
% This format preserves all bare FITS keywords (with no value field) and
% all comments.
%
% Header format #3: The header may be a MATLAB structure in the format
% header.keyword1=value1;
% header.keyword2=value2;
% header.keyword3=value3;
% 
% This format is compatible with the MFITSIO package.  Note that it does
% not permit bare keywords or comment fields.
%
% Certain mandatory FITS fields may be added of they are not present in
% the supplied header.  Please see the function 'requiredKeywords' in this
% file for details.  These required keywords will be used to generate a
% minimal FITS header if the optional header field is not supplied.
%
% ==============================
% Modify these lines to suit your data and machine architecture.
%
% FITS file BITPIX field.  Common choices are -32 for single precision
% data, and -64 for double precision.
BITPIX=-32; % [-32, -64]
%
% machineFormat describes the "endian-ness" of your machine.  Valid choices
% are 'native', 'L', 'B', and others described in MATLAB's documentation
% for 'fopen'.  Type 'help fopen' on the command line for details.
% *** N.B. NOTE THE MATLAB'S FITSREAD COMMAND ALWAYS USES machineFormat='B'.
% THIS OPTION IS THEREFORE REQUIRED FOR COMPATIBILITY WITH FITSREAD.M.
% USE WITH OTHER FITSREADERS MAY REQUIRE OTHER OPTIONS. ***
machineFormat='B'; % ['native', 'L', 'B']
%
% MATLAB's built-in FITS reader seems to transpose the first 2 dimensions
% (Rows and Columns) of a multi-dimensional data set.  Set 'transposeRC=true'
% to write files that are compatible with this behaviour, or 'transposeRC=false' if
% you do not wish to transpose.  If your data seems to have the X and Y
% axis transposed when viewed with an external viewer, then you have
% probably made the wrong choice.
transposeRC=true;
% ==============================

% Retrieve or initialize the header.
if isempty(varargin)
    header=[];
else
    header=varargin{1};
end

% Check if the header is in format #1.  Extract the required keyword/value
% cell array if it is.
if isstruct(header) && isfield(header, 'PrimaryData') &&...
        isfield(header.PrimaryData, 'Keywords')
    header=header.PrimaryData.Keywords;
end

% Obtain the size of the data and transpose rows and columns for
% compatibility with fitsread if requested.
szData=size(data);
if length(szData) > 1 && transposeRC
    order=1:length(szData);
    order(1:2)=[2,1];
    data=permute(data, order);
end

% Generate an N x 80 character array hStr containing the FITS header
% information.  'struct2str' is called if the supplied header is a
% structure, or 'cell2str' if it is in cell array format.
if isstruct(header)
    [hStr, hReq]=struct2str(header, szData);
elseif iscell(header)
    [hStr, hReq]=cell2str(header, szData);
else
    % No header was supplied or the format is wrong.
    hStr=[];
    hReq=requiredKeywords(szData);
end
% Mandatory header fields *that were not over-written by the supplied
% header* are returned in the hReq.  They are prepended to the FITS
% character array below.
%
% Add the mandatory 'END' line.
hStr=addLine(hStr, 'END');

% Add any remaining mandatory fields that have not already been
% over-written by the supplied header.
if ~isempty(hReq)
    hStr=[struct2str(hReq, szData); hStr];
end

% Determine the 'precision' string that corresponds to the BITPIX field.
prec=getPrecision;

% Append the file extension '.fits' if the supplied name does not already
% end in '.fits' or '.FITS'.

% commented by Marcelo, regexpi is a non found function
%if isempty(regexpi(filename, '\.fits$'))
%    filename=[filename, '.fits'];
%end

fid=fopen(filename, 'w+', machineFormat); % Open file for writing.
fwrite(fid, hStr', 'char'); % Write the header.
%
% Pad the header with 8-bit zeros to satisfy the FITS format requirement
% that data are written to 2880 bye blocks.
padFile(fid, 8, machineFormat);
%
fwrite(fid, data, prec, 0, machineFormat); % Write the image data.
%
% Pad the image with zeros (of the same BITPIX format) to satisfy the FITS
% format requirement that data are written to 2880 bye blocks.
padFile(fid, BITPIX, machineFormat)
fclose(fid);


