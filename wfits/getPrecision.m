function prec=getPrecision(varargin)
global BITPIX
% Determine the precision string from the BITPIX field.  This is used by
% fopen and fwrite.
if isempty(varargin)
    BPX=BITPIX;
else
    BPX=varargin{1};
end
%
switch BPX
    case 8
        prec='uint8';
    case 16
        prec='int16';
    case 32
        prec='int32'; %  'integer*4'
    case 64
        prec='int64'; % 'integer*8'
    case -32
        prec='single'; % 'float32'; % real*4
    case -64
        prec='double'; % 'float64'; % real *8
    otherwise
        disp('Invalid BITPIX value.');
        BITPIX=-32;
        prec='single'; % 'float32'; % real*4
end