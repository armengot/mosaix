function [hLine, hReq]=makeHeaderLine(keyword, value, comment, hReq)
% Generate a single line of the FITS header.  hReq is a structure containing the
% list of mandatory keywords in the FITS standard.  Fields are removed from
% hReq as they are encountered and over-written using the supplied header
% information.
%
% Get the keyword and check that is is not 'END'.  The mandatory 'END'
% keyword is added automatically.  The ensures that the 'END' keyword is
% not duplicated.
keyword=upper(strtrim(char(keyword)));
if strcmpi(keyword, 'END')
    hLine=[];
    return
end
%
% Trim the keyword to the allowed 8 bytes if it is too long.
lenKey=length(keyword);
if lenKey > 8
    keyword=keyword(1:8);
    lenKey=8;
end
%
% Check whether keyword is in the list of required keywords.  If it is,
% then remove the keyword from hReq.
if isfield(hReq, keyword)
    value=hReq.(keyword);
    hReq=rmfield(hReq, keyword);
end
%
% Validate the value field.  This converts logicals to 'T' or 'F', and
% converts numeric data to strings.  The FITS standard allows a maximum of
% 70 characters for the value field.  The value is truncated if this is
% exceeded.
value=validate(value);
lenValue=length(value);
if lenValue > 70
    value=value(1:70);
    lenValue=70;
end
%
% If there is a comment field supplied, make sure that it is prepended by a
% space and a '/' character.  The comment is truncated if the entire line
% extends past 80 characters.
comment=strtrim(comment);
if any(comment) && comment(1) ~= '/'
    comment=[' /', comment];
end
lenComment=min(length(comment), 70-lenValue);
comment=comment(1:lenComment);
%
hLine=repmat(' ', 1, 80);
hLine(1:lenKey)=keyword;
if lenValue > 0
    hLine(9:10)='= ';
    hLine(11:11+lenValue-1)=value;
end
if lenComment > 0
    hLine(11+lenValue:10+lenValue+lenComment)=comment;
end