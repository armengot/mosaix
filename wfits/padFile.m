function padFile(fid, BITPIX, machineFormat)
	 % Pad the file with zeros - FITS standard requires block sizes of 2880 bytes.
	 modBlock=mod(ftell(fid),2880);
	 if modBlock > 0
	    NPad=(2880-modBlock)/(abs(BITPIX)/8);
	    padding=zeros(1,NPad);
	    fwrite(fid, padding, getPrecision(BITPIX), 0, machineFormat);
	 end
