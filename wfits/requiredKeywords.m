function h=requiredKeywords(szData)
global BITPIX transposeRC
% The list of mandatory keywords specified by the FITS standard.
%
h.SIMPLE=true;
h.BITPIX=BITPIX;
h.NAXIS=length(szData);
%
% Swap axes 1 & 2 if the axes are transposed.
if h.NAXIS > 1 && transposeRC
    szData([2,1])=szData([1,2]);
end
%
% Generate the keywords for NAXIS1, NAXIS2, etc.
for i=1:h.NAXIS
    h.(['NAXIS', num2str(i)])=szData(i);
end