function  [hStr, hReq]=struct2str(header, szData)
% Generates a FITS header from a MATLAB structure in the same format
% used by mfitsio.  hReq is a list of *unused* mandatory fields
% in the FITS standard.
keywords=fields(header);
nr=length(keywords);
% No comments or bare keywords are possible when using structures.
hReq=requiredKeywords(szData);
hStr='';
for i=1:nr
    keyword=keywords{i};
    value=header.(keyword);
    %
    % Generate a single header line and append it to hStr.  Note that the
    % mandatory FITS fields in hReq are removed as they are used.  This
    % will only contain unused fields at the end.
    [hLine, hReq]=makeHeaderLine(keyword, value, '', hReq);
    hStr=[hStr; hLine]; %#ok
end