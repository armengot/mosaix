function value=validate(value)
% Formats the 'value' field.
if isempty(value)
    value='';
elseif ischar(value)
    % Make sure that the value string is surrounded by single quotes.
    if value(1) == ''''
        value(1)=[];
    elseif value(end) == ''''
        value(end)=[];
    end
    value=['''', strtrim(value), ''''];
elseif islogical(value)
    % Convert logicals to 'T' or 'F'.
    if value
        value='T';
    else
        value='F';
    end
elseif isnumeric(value)
    % Convert numerical values to a string representation.
    value=num2str(value);
end
value=upper(strtrim(value));