function [imax] = zmax(data_header)
% ZMAX
%   check square matrix image
%
	ind_naxis1=cellindexnonull(strfind(data_header(:,1),'NAXIS1'));
	ind_naxis2=cellindexnonull(strfind(data_header(:,1),'NAXIS2'));
	tmpN1=data_header(ind_naxis1,2);
	tmpN2=data_header(ind_naxis2,2);
	N1=tmpN1{1};
	N2=tmpN2{1};
    if (N1~=N2)
        display(' - Be careful. Non equal axis.');
    else
        imax=N1;
    end
end

