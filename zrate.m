function [rate] = zrate(data_header)
% ZRATE
%   extracts CDELT2 keyword from a FITS hdu
	ind_cd=cellindexnonull(strfind(data_header(:,1),'CDELT2'));	
	tmp_cd=data_header(ind_cd,2);
	rate=tmp_cd{1};    
end

